# Tor-scripts

A place for storing and publishing the scripts I use for setting up tor relays under Debian/Ubuntu.
The script for setting up a bridge works fine.
It adds the tor repository from the torproject, configures unattended updates with reboots and configures tor.
For setting up a (non-)exit-relay I have to find time.

## My sources

This is no rocket-science I mostly copy&pasted informations/commands from the following websites:
- https://community.torproject.org/relay/setup/bridge/debian-ubuntu/
- https://support.torproject.org/apt/tor-deb-repo/
- https://community.torproject.org/relay/setup/guard/debian-ubuntu/updates/

## How to run the scripts

Just download it and make it executable 
`chmod +x scriptname` //replace scriptname with the full filename including .sh

And run it with `sudo scriptname`


## Contributing
If you want to contribute, you are welcome!
You can work on the script or test it on other operating systems.
All you need to know is to know what you are doing.

## Authors and acknowledgment
At the moment it's juts me.

## License
Licensed under Apache License 2.0 (ASL 2.0)

## Project status
I tested it with debian 11 and 12 on which it works great.
