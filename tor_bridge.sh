#!/bin/bash

# In this part we set/chose variables.
# bname and maddress are required to identify the bridge and that you can be reaced by the devs if necessary.
read -p "What sould be the name of the bridge (only one word))? " bname
read -p "Which mail address should be visible (only one word and disguise it because of spam @ -> [at]))? " maddress
# with the following line we read the codename from your debian relase and assign it to the variable codename which we need for the tor-repo.
echo "Reading the codename of your realease (required later)"
codename=$(. /etc/os-release; echo $VERSION_CODENAME)
echo "The codename of your release is:"
echo $codename
# ORPort and obfs4Port are required for the bridge to be reachable. 
# This script uses a fixed range and picks a value, if you want to set it manualy and if you know what you do, feel free.
echo "Generating random Ports for ORPort and obfs4"
DIFF=$((9000-5000+1))
ORPort=$(($(($RANDOM%$DIFF))+5000))
obfs4Port=$(($(($RANDOM%$DIFF))+5000))

# Updating and upgrading the os.
echo "Updating and upgrading the os."
sudo apt update
sudo apt upgrade -y

# Installing unattended-upgrades.
echo "Installing and configuring unattend-upgrades"
sudo apt install -y unattended-upgrades apt-listchanges apt-transport-https

# Configuring unattende-upgrades.
sudo echo "Unattended-Upgrade::Origins-Pattern {
    "origin=Debian,codename=${distro_codename},label=Debian-Security";
    "origin=TorProject";
};
Unattended-Upgrade::Package-Blacklist {
};" >> /etc/apt/apt.conf.d/50unattended-upgrades
sudo echo "APT::Periodic::Update-Package-Lists "1";
APT::Periodic::AutocleanInterval "5";
APT::Periodic::Unattended-Upgrade "1";
APT::Periodic::Verbose "1";" >> /etc/apt/apt.conf.d/20auto-upgrades
# Allowing unattended-upgrades to reboot your relay.
sudo echo 'Unattended-Upgrade::Automatic-Reboot "true";' >> /etc/apt/apt.conf.d/50unattended-upgrades

# Adding and signing the tor-repo from the torproject to have the latest stable version available.
echo "Adding the tor-repo"
sudo echo "deb     [signed-by=/usr/share/keyrings/tor-archive-keyring.gpg] https://deb.torproject.org/torproject.org $codename main
deb-src [signed-by=/usr/share/keyrings/tor-archive-keyring.gpg] https://deb.torproject.org/torproject.org $codename main" >> /etc/apt/sources.list.d/tor.list
sudo wget -qO- https://deb.torproject.org/torproject.org/A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89.asc | sudo gpg --dearmor | sudo tee /usr/share/keyrings/tor-archive-keyring.gpg >/dev/null

# Updating your sources and installing tor, the keyring, obfs4proxy (required for the bridge) and nyx (to monitor the relay).
echo "Updating and installing tor and other required software"
sudo apt update
sudo apt install -y tor deb.torproject.org-keyring obfs4proxy nyx

# Generating the /etc/tor/torrc which configures your relay.
echo "generating the config file /etc/tor/torrc"
sudo echo "BridgeRelay 1

# Replace $ORPort with a Tor port of your choice if you want so.
# This port must be externally reachable.
# Avoid port 9001 because it's commonly associated with Tor and censors may be scanning the Internet for this port.
ORPort $ORPort

ServerTransportPlugin obfs4 exec /usr/bin/obfs4proxy

# Replace $obfs4Port with an obfs4 port of your choice if you want so.
# This port must be externally reachable and must be different from the one specified for ORPort.
# Avoid port 9001 because it's commonly associated with Tor and censors may be scanning the Internet for this port.
ServerTransportListenAddr obfs4 0.0.0.0:$obfs4Port

# Local communication port between Tor and obfs4.  Always set this to auto.
# Ext means extended, not external.  Don't try to set a specific port number, nor listen on 0.0.0.0.
ExtORPort auto

# Replace <$maddress> with your email address so the devs can contact you if there are problems with your bridge.
# This is optional but encouraged.
ContactInfo <$maddress>

# Pick a nickname that you like for your bridge.  This is optional.
Nickname $bname

#Entries for running NYX
ControlPort 9051
CookieAuthentication 1" > /etc/tor/torrc

# Enabling tor at startup/reboot
echo "Enabling tor at startup/reboot"
sudo systemctl enable --now tor.service
sudo systemctl restart tor.service

#restarting your relay
echo "restarting relay"
sudo shutdown now -r
